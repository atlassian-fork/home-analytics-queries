/*

Query to find out the number of distinct users who have done specific actions
on a weekly basis

Easy to add more actions if needed

Author: Josh Devenny (jdevenny)
*/

--- number of distinct users performing key actions in Home each WEEK
select
  date_trunc('week', cast(day as date)) as week
  ,count(distinct case when event = 'atlassian.home.notifications.item.click' then instance||username end) as notification_item_clicked
  ,count(distinct case when event = 'atlassian.home.notifications.view' then instance||username end) as notifications_viewed
  ,count(distinct case when event = 'atlassian.home.people-search.click' then instance||username end) as people_search_click
  ,count(distinct case when event = 'atlassian.home.profile-page.view' then instance||username end) as profile_page_view
  ,count(distinct case when event = 'atlassian.home.newsfeed.view' then instance||username end) as newsfeed_viewed
  ,count(distinct case when event = 'atlassian.home.newsfeed.item.click' then instance||username end) as newsfeed_item_click
from raw_product.cloud_event_summary
where
  product = 'home'
  and instance not like '%fabric%'
  and instance not like '%.jira-dev.com'
  and instance != 'localhost'
  and date_trunc('week', cast(day as date)) >= current_date - interval '90' day
  and date_trunc('week', cast(day as date)) < date_trunc('week', current_date)
group by 1
order by 1;