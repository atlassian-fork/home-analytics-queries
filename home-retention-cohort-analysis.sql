/*

Query to bucket users into weekly cohorts, which can then be used in
Tableau to create a cohort analysis

Author: Josh Devenny (jdevenny)
*/

-- INFO: model.dim_date week's start on Sunday, date_trunc('week', x) function in Presto truncates to Monday...

with new_users as (
    -- we only care about NEW users to Home, so we can put them into cohorts broken up by week
    select sen_username, min(date_trunc('week', cast(utc_date as date))) as first_week_active
    from raw_product.active_users
    where 
        product = 'home'
        and metric = 'wau'
        and component = 'new'
        and cast(utc_date as date) >= date('2017-08-27')
    group by 1

), weeks as (
    select distinct date_trunc('week',cast(date_week as date)) as week from model.dim_date
    where date_week >= date('2017-08-27') and date_week <= current_date
    order by 1
), new_users_by_week as (
    -- need to do a cross join to make sure that we have an entry EVERY week for each username
    select 
        w.week, sen_username, n.first_week_active
    from weeks w
    cross join new_users n
), active_weeks as (
    -- need to do distinct otherwise we'll get a row back for every time they were a WAU within the 7 day period (we don't care if it's > 1)
    select distinct date_trunc('week', cast(utc_date as date)) as week_active, sen_username
    from raw_product.active_users
    where 
        product = 'home'
        and metric = 'dau'
        and component != 'dormant'
        and cast(utc_date as date) >= date('2017-08-27')
        and sen_username in (select sen_username from new_users)
)
select
    n.week -- model.dim_date week's start on Sunday, date_trunc('week', x) function in Presto truncates to Monday...
    , n.sen_username
    , n.first_week_active
    , sum(case when a.week_active is not null then 1 else 0 end) as active
from new_users_by_week n
left join active_weeks a on a.sen_username = n.sen_username and date_trunc('week', cast(a.week_active as date)) = cast(n.week as date) -- join on the week AND username, and we'll be able to tell whether that user was active or not in that week
group by 1, 2, 3
order by 1, 2
;